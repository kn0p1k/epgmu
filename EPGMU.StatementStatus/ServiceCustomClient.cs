﻿using EPGMU.StatementStatus.EventService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EPGMU.StatementStatus
{
    public class ServiceCustomClient
    {
        private EventServiceClient client;
        public readonly Uri ServiceUri;
        //public readonly string Action;

        public ServiceCustomClient()
        {
            client = new EventServiceClient();
            HeaderType header = new HeaderType();
            ServiceUri = client.Endpoint.Address.Uri;
        }

        public XmlDocument SendMessage(XmlDocument doc, string methodName = "pushEvent")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ServiceUri);
            request.Credentials = CredentialCache.DefaultCredentials;
            var action = string.Concat(client.Endpoint.Address.Uri, "/", methodName);
            request.Headers.Add("SOAPAction", action);
            request.ContentType = "text/xml;charset=\"utf-8\"";
            request.Method = "POST";
            request.Timeout = 10000;
            try
            {
                Debug.WriteLine("----Proxy address: " + ((WebProxy)WebRequest.DefaultWebProxy).Address);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("----Error on getting proxy address: " + ex.Message);
            }

            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(doc.OuterXml);
            Debug.WriteLine("----Sending XML: " + doc.OuterXml);
            writer.Close();
            request.GetRequestStream().Flush();
            int err = 0;
            string errDescription = string.Empty;
            HttpWebResponse response;
            Debug.WriteLine("----Request ready");
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException wex)
            {
                Debug.WriteLine("----Web request exception: " + wex.Message);
                err = 10001;
                errDescription = wex.Message;
                response = (HttpWebResponse)wex.Response;
            }
            
            XmlDocument respDoc = new XmlDocument();
            respDoc.PreserveWhitespace = true;
            respDoc.Load(response.GetResponseStream());
            response.Close();
            Debug.WriteLine("----Request successfull, response: " + respDoc.OuterXml);
            return respDoc;
        }
    }
}
