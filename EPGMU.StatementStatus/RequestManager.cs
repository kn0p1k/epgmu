﻿using EPGMU.StatementStatus.EventService;
using EPGMU.StatementStatus.Properties;
using GostCryptography.Cryptography;
using GostCryptography.Xml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace EPGMU.StatementStatus
{
    public static class RequestManager
    {
        private const string WsSecurityExtNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        private const string WsSecurityUtilityNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
        private static ServiceCustomClient serviceClient = new ServiceCustomClient();
        private static XmlSerializer xsSubmit = new XmlSerializer(typeof(PushEventRequest));

        public static XmlDocument GetResponse(PushEventRequest request)
        {
            var cert = CertificateHelper.GetCertificate();
            var xmlDocument = EnvelopMessage(request);
            var signedMessage = SignSmevRequest(xmlDocument, cert);
            var res = serviceClient.SendMessage(signedMessage);
            return res;
        }

        public static XmlDocument GetResponse(string request)
        {
            Debug.WriteLine("--Getting certificate");
            var cert = CertificateHelper.GetCertificate();
            Debug.WriteLine("--Enveloping message");
            var xmlDocument = EnvelopMessage(request);
            Debug.WriteLine("--Signing request");
            Debug.WriteLine("--Signing request certificate thumprint: " + cert.Thumbprint);
            var signedMessage = SignSmevRequest(xmlDocument, cert);
            Debug.WriteLine("--Starting web request...");
            var res = serviceClient.SendMessage(signedMessage);
            Debug.WriteLine("--Returning result");
            return res;
        }

        private static XmlDocument EnvelopMessage(PushEventRequest message)
        {
            var fullDoc = new XmlDocument();
            var xmlText = Resources.SoapEnvelop.Replace("#body_content", SerializeRequestObject(message));
            fullDoc.LoadXml(xmlText);
            return fullDoc;
        }

        private static XmlDocument EnvelopMessage(string message)
        {
            var fullDoc = new XmlDocument();
            var xmlText = Resources.SoapEnvelop.Replace("#body_content", message);
            fullDoc.LoadXml(xmlText);
            return fullDoc;
        }

        private static string SerializeRequestObject(object obj)
        {
            var namespaces = new XmlSerializerNamespaces(new[] { new XmlQualifiedName("rev", "http://smev.gosuslugi.ru/rev120315"), new XmlQualifiedName("v25", "http://idecs.atc.ru/orderprocessing/ws/eventservice/v25/") });
            var settings = new XmlWriterSettings()
            {
                Indent = true,
                OmitXmlDeclaration = true
            };
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
            {
                xsSubmit.Serialize(xmlTextWriter, obj, namespaces);
                xmlTextWriter.Flush();
                return stringWriter.ToString();
            }
        }

        private static XmlElement GetSmevIdElement(XmlDocument document, string idValue)
        {
            var namespaceManager = new XmlNamespaceManager(document.NameTable);
            namespaceManager.AddNamespace("wsu", WsSecurityUtilityNamespace);

            return document.SelectSingleNode("//*[@wsu:Id='" + idValue + "']", namespaceManager) as XmlElement;
        }

        private static XmlDocument SignSmevRequest(XmlDocument smevRequest, X509Certificate2 signingCertificate)
        {
            // Создание подписчика XML-документа
            var signedXml = new GostSignedXml(smevRequest) { GetIdElementHandler = GetSmevIdElement };

            // Установка ключа для создания подписи
            signedXml.SetSigningCertificate(signingCertificate);

            // Ссылка на узел, который нужно подписать, с указанием алгоритма хэширования ГОСТ Р 34.11-94 (в соответствии с методическими рекомендациями СМЭВ)
            var dataReference = new Reference { Uri = "#body", DigestMethod = GostSignedXml.XmlDsigGost3411ObsoleteUrl };

            // Метод преобразования, применяемый к данным перед их подписью (в соответствии с методическими рекомендациями СМЭВ)
            var dataTransform = new XmlDsigExcC14NTransform();
            dataReference.AddTransform(dataTransform);

            // Установка ссылки на узел
            signedXml.AddReference(dataReference);

            // Установка алгоритма нормализации узла SignedInfo (в соответствии с методическими рекомендациями СМЭВ)
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

            // Установка алгоритма подписи ГОСТ Р 34.10-2001 (в соответствии с методическими рекомендациями СМЭВ)
            signedXml.SignedInfo.SignatureMethod = GostSignedXml.XmlDsigGost3410ObsoleteUrl;

            // Вычисление подписи
            signedXml.ComputeSignature();

            // Получение XML-представления подписи
            var signatureXml = signedXml.GetXml();

            // Добавление подписи в исходный документ
            smevRequest.GetElementsByTagName("ds:Signature")[0].PrependChild(smevRequest.ImportNode(signatureXml.GetElementsByTagName("SignatureValue")[0], true));
            smevRequest.GetElementsByTagName("ds:Signature")[0].PrependChild(smevRequest.ImportNode(signatureXml.GetElementsByTagName("SignedInfo")[0], true));
            smevRequest.GetElementsByTagName("wsse:BinarySecurityToken")[0].InnerText = Convert.ToBase64String(signingCertificate.RawData);

            return smevRequest;
        }

        private static bool VerifySmevRequestSignature(XmlDocument signedSmevRequest)
        {
            // Создание подписчика XML-документа
            var signedXml = new GostSignedXml(signedSmevRequest) { GetIdElementHandler = GetSmevIdElement };

            // Поиск узла с подписью
            var nodeList = signedSmevRequest.GetElementsByTagName("Signature", SignedXml.XmlDsigNamespaceUrl);

            // Загрузка найденной подписи
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Поиск ссылки на BinarySecurityToken
            var references = signedXml.KeyInfo.GetXml().GetElementsByTagName("Reference", WsSecurityExtNamespace);

            if (references.Count > 0)
            {
                // Определение ссылки на сертификат (ссылка на узел документа)
                var binaryTokenReference = ((XmlElement)references[0]).GetAttribute("URI");

                if (!String.IsNullOrEmpty(binaryTokenReference) && binaryTokenReference[0] == '#')
                {
                    // Поиск элемента с закодированным в Base64 сертификатом
                    var binaryTokenElement = signedXml.GetIdElement(signedSmevRequest, binaryTokenReference.Substring(1));

                    if (binaryTokenElement != null)
                    {
                        // Загрузка сертификата, который был использован для подписи
                        var signingCertificate = new X509Certificate2(Convert.FromBase64String(binaryTokenElement.InnerText));

                        // Проверка подписи
                        return signedXml.CheckSignature(signingCertificate.GetPublicKeyAlgorithm());
                    }
                }
            }

            return false;
        }

    }
}
