﻿using GostCryptography.Cryptography;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace EPGMU.StatementStatus
{
    public static class CertificateHelper
    {
        public readonly static string CertThumbprint = ConfigurationManager.AppSettings["CertThumbprint"];
        
        /// <summary>
        /// Имя хранилища для поиска тестового сертификата.
        /// </summary>
        /// <remarks>
        /// Значение равно <see cref="StoreName.My"/>.
        /// </remarks>
        public const StoreName CertStoreName = StoreName.My;

        /// <summary>
        /// Местоположение для поиска тестового сертификата.
        /// </summary>
        /// <remarks>
        /// Значение равно <see cref="StoreLocation.LocalMachine"/>.
        /// </remarks>
        public const StoreLocation CertStoreLocation = StoreLocation.LocalMachine;

        private static readonly X509Certificate2 GostCetificate = FindGostCertificate();

        public static CspParameters GetKeyContainer()
        {
            return GostCetificate.GetPrivateKeyInfo();
        }

        public static X509Certificate2 GetCertificate()
        {
            return GostCetificate;
        }

        private static X509Certificate2 FindGostCertificate()
        {
            var store = new X509Store(CertStoreName, CertStoreLocation);
            store.Open(OpenFlags.ReadOnly);

            try
            {
                //var searchRes = store.Certificates.Find(X509FindType.FindByThumbprint, CertThumbprint, false);
                //if (searchRes.Count > 0 && searchRes[0].HasPrivateKey)
                //{
                //    return searchRes[0];
                //}
                //if (searchRes.Count == 0)
                //{
                    foreach (var certificate in store.Certificates)
                    {
                        if (certificate.HasPrivateKey && certificate.SignatureAlgorithm.Value == "1.2.643.2.2.3")
                        {
                            return certificate;
                        }
                    }
                //}
            }
            finally
            {
                store.Close();
            }

            return null;
        }
    }
}
