﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EPGMU.Service.Common
{
    public class PaymentStatusParams: RequestCommonParams
    {
        /// <summary>
        /// Состояние оплаты: «W» - ожидает оплаты, «P» - оплачено
        /// </summary>
        [Display(Name = "Статус")]
        public string Status { get; set; }

        /// <summary>
        /// Источник начисления. Для status=«W» должно быть хотя бы одно начисление. Для ФК указывается значение «FK».
        /// </summary>
        [Display(Name = "Источник начисления")]
        public string Source { get; set; }

        /// <summary>
        /// Уникальный идентификатор начисления. Для status=«W» должно быть хотя бы одно начисление
        /// </summary>
        [Display(Name = "Уникальный идентификатор начисления")]
        public string Uin { get; set; }

        /// <summary>
        /// Назначение платежа. Для status=«W» должно быть хотя бы одно начисление. Выводится в заявлении в поле «Наименования платежа» вкладки «Счета к оплате»
        /// </summary>
        [Display(Name = "Назначение платежа")]
        public string Description { get; set; }
    }
}