﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EPGMU.Service.Common
{
    public class InformationMessageParams: RequestCommonParams
    {
        /// <summary>
        /// Код информационного сообщения
        /// </summary>
        [Display(Name = "Код")]
        public string Code { get; set; }
    }
}