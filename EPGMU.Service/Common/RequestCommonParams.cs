﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EPGMU.Service.Common
{
    public class RequestCommonParams
    {
        /// <summary>
        /// Уникальный идентификатор заявления на ЕПГУ
        /// </summary>
        [Display(Name = "Идентификатор заявления")]
        public long OrderId { get; set; }

        /// <summary>
        /// Дата и время события (Если не указано, то на ЕПГУ будет выводиться системная дата получения события)
        /// </summary>
        public DateTime? EventDate { get; set; }

        /// <summary>
        /// Комментарий к событию (Выводится в заявлении в поле «Комментарий» вкладки «История рассмотрения заявления»)
        /// </summary>
        [Display(Name = "Комментарий")]
        public string EventComment { get; set; }

        /// <summary>
        /// Автор (Выводится в заявлении в поле «Автор» вкладки «История рассмотрения заявления»)
        /// </summary>
        [Display(Name = "Автор")]
        public string EventAutor { get; set; }
    }
}