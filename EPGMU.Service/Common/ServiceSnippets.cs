﻿using EPGMU.StatementStatus.EventService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPGMU.Service.Common
{
    public static class ServiceSnippets
    {
        public static MessageType GetMessageHeader()
        {
            return new MessageType()
            {
                Sender = new orgExternalType
                {
                    Code = "043T01891",
                    Name = "ИС Единая информационная система по моделированию и прогнозированию социально - экономического развития коренных малочисленных народов Севера Ямало - Ненецкого автономного округа"
                },
                Recipient = new orgExternalType
                {
                    Code = "IPGU01001",
                    Name = "Единый портал государственных услуг"
                },
                Originator = new orgExternalType
                {
                    Code = "043T01891",
                    Name = "ИС Единая информационная система по моделированию и прогнозированию социально - экономического развития коренных малочисленных народов Севера Ямало - Ненецкого автономного округа"
                },
                TypeCode = TypeCodeType.GSRV,
                Status = StatusType.RESULT,
                Date = DateTime.Now,
                ExchangeType = "1", // Категория взаимодействия
                ServiceCode = "10001970310", // Код государственной услуги, в рамках оказания которой осуществляется информационный обмен
                CaseNumber = "86980805", // Номер дела в информационной системе-отправителе
                TestMsg = string.Empty
            };
        }

        public static string ProcessResponse()
        {
            return "asd";
        }

    }
}