﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EPGMU.Service.Common
{
    public class StatementStatusParams: RequestCommonParams
    {
        /// <summary>
        /// Технологический код статуса на ЕПГУ (перечень технологических кодов приведен в руководстве пользователя сервиса)
        /// </summary>
        [Display(Name = "Статус")]
        public long StatusCode { get; set; }

        /// <summary>
        /// Возможность запроса Заявителем отмены заявления. 
        /// </summary>
        [Display(Name = "Отмена заявления")]
        public bool CancelAllowed { get; set; } = false;

        /// <summary>
        /// Возможность посылки Заявителем текстовых сообщений в ИС государственного органа (ведомства).
        /// </summary>
        [Display(Name = "Посылка текстовых сообщений")]
        public bool SendMessageAllowed { get; set; } = false;
    }
}