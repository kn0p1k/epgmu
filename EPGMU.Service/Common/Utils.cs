﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;
using System.Data;

namespace EPGMU.Service.Common
{
    /// <summary>
    /// Сборник полезных функций
    /// </summary>
    public class Utils
    {
        private static string OracleDBUser = ConfigurationManager.AppSettings["OracleDBUser"];
        private static string OracleDBPassword = ConfigurationManager.AppSettings["OracleDBPassword"];
        private static string connectionString = string.Format( "User Id={0};Password={1};Data Source=oracleDataSource;", OracleDBUser, OracleDBPassword);
        /// <summary>
        /// Очищает строку от "запрещенных" символов
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string clearString(string str)
        {
            return str.Replace("\"", "'")
                        .Replace("*", "")
                        .Replace("|", "")
                        .Replace("\\", "")
                        .Replace(":", "")
                        .Replace("<", "")
                        .Replace(">", "")
                        .Replace("?", "")
                        .Replace("/", "");
        }

        /// <summary>
        /// Выполняет sql-оператор или pl/sql блок
        /// </summary>
        /// <param name="query">текст оператора или блока</param>
        /// <returns>Количество обработанных строк</returns>
        public static void ExecuteQuery(string query)
        {
            using (OracleConnection Connection = new OracleConnection(connectionString))
            {
                try
                {
                    Connection.Open();
                    OracleCommand SQLCommand = new OracleCommand();
                    SQLCommand.Connection = Connection;
                    SQLCommand.CommandText = query;
                    SQLCommand.ExecuteNonQuery();
                }
                finally
                {
                    Connection.Close();
                }
            }
        }

        /// <summary>
        /// Возвращает единичный результат запроса
        /// </summary>
        /// <param name="query">текст запроса</param>
        /// <returns></returns>
        public static object GetQueryResult(string query)
        {
            object vResult = DBNull.Value;
            using (OracleConnection Connection = new OracleConnection(connectionString))
            {
                try
                {
                    Connection.Open();
                    OracleCommand SQLCommand = new OracleCommand();
                    SQLCommand.Connection = Connection;
                    SQLCommand.CommandText = query;
                    OracleDataReader reader = SQLCommand.ExecuteReader();
                    if (reader.Read())
                    {
                        vResult = reader.GetOracleValue(0);
                    }
                    reader.Close();
                }catch(Exception e) {
                    throw e;
                }
                finally
                {
                    Connection.Close();
                }
            }
            return vResult;
        }

        /// <summary>
        /// Возвращает DataTable по результату запроса query
        /// </summary>
        /// <param name="query">текст запроса</param>
        /// <returns>DataTable</returns>
        public static DataTable GetDataTable(string query)
        {
            DataTable ResultData = new DataTable();
            using (OracleConnection Connection = new OracleConnection(connectionString))
            {
                OracleDataAdapter DataAdapter = new OracleDataAdapter();
                OracleCommand SQLCommand = new OracleCommand();

                try
                {
                    SQLCommand.Connection = Connection;
                    DataAdapter.SelectCommand = SQLCommand;

                    SQLCommand.CommandText = query;
                    DataAdapter.Fill(ResultData);
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    Connection.Close();
                }
            }

            return ResultData;
        }
    }
}