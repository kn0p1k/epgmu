﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace EPGMU.Service.Common
{
    public static class XmlHelper
    {
        public static string FormatXML(XmlDocument document)
        {
            string result = "";
            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            try
            {
                writer.Formatting = Formatting.Indented;
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                mStream.Position = 0;

                StreamReader sReader = new StreamReader(mStream);

                string FormattedXML = sReader.ReadToEnd();

                result = FormattedXML;
            }
            catch (Exception ex)
            {
                return "Ошибка при форматировании XML ответа: " + ex.Message;
            }
            return result;
        }
    }
}