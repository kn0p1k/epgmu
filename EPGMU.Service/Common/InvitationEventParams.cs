﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EPGMU.Service.Common
{
    public class InvitationEventParams: RequestCommonParams
    {
        /// <summary>
        /// Код приглашения в ИС ведомства. Используется в дальнейшем для изменения или удаления приглашения.
        /// </summary>
        [Display(Name = "Код приглашения")]
        public string Code { get; set; }

        /// <summary>
        /// Действие: ADD - Добавление, UPDATE - Изменение, CANCEL - Отмена. 
        /// </summary>
        [Display(Name = "Действие")]
        public string Action { get; set; }

        /// <summary>
        /// Наименование отделения, офиса ОИВ. Обязательно для заполнения, если «action» = «ADD» или «UPDATE»
        /// </summary>
        [Display(Name = "Наименование ОИВ")]
        public string OrgName { get; set; }

        /// <summary>
        /// Адрес ОИВ, включая офис, кабинет. Обязательно, если «action» = «ADD» или «UPDATE».
        /// </summary>
        [Display(Name = "Адрес ОИВ")]
        public string Address { get; set; }

        /// <summary>
        /// Дата и время начала. Обязательно, если «action» = «ADD» или «UPDATE».
        /// </summary>
        [Display(Name = "Дата и время начала")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата и время окончания. Необязательно для заполнения.
        /// </summary>
        [Display(Name = "Дата и время окончания")]
        public DateTime? EndDate { get; set; }
    }
}