﻿$(function () {
    var apiPath = window.location.origin + "/api/";
    $("#btnSendSample").bind("click", function () {
        $.ajax({
            method: "GET",
            url: apiPath + "TestApi",
            success: function (response) {
                $("#responseText").text(response);
            },
            error: function (a, b) {
                $("#responseText").text(a);
            }
        });
    });
    $("#btnStatusChange").bind("click", function () {
        debugger;
        $.ajax({
            method: "GET",
            url: apiPath + "StatementStatus",
            success: function (response) {
                $("#responseText").text(response);
            },
            error: function (a, b) {
                $("#responseText").text(a);
            }
        });
    });
    $("#btnSendRequest").bind("click", function () {
        var requestObj = "{ 'requestText': '" + $("#requestText").text() + "' }";//я устал бороться с ним..
        $.ajax({
            method: "POST",
            url: apiPath + "TestApi/GetResponse",
            contentType: "application/json",
            data: requestObj,
            success: function (response) {
                $("#responseText").text(response);
            },
            error: function (a, b) {
                $("#responseText").text(a);
                //console.log(a);
            }
        });
    });
    $("#btnCleanResponse").bind("click", function () {
        $("#responseText").text("");
    });
    
});