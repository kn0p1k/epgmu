﻿using EPGMU.Service.Common;
using EPGMU.StatementStatus;
using EPGMU.StatementStatus.EventService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace EPGMU.Service.Controllers
{
    public class InvitationEventController : ApiController
    {
#if DEBUG
        /// <summary>
        /// Тест метода Post
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Post(new InvitationEventParams
            {
                OrderId = 86980805,
                EventComment = "Comment",
                EventAutor = "Autor",
                Code = "1",
                Action = "ADD",
                OrgName = "Название",
                Address = "Адрес",
                StartDate = new DateTime(2015, 4, 10, 19, 10, 00)
            });
        }
#endif
        /// <summary>
        /// Передача текстового сообщения по заявлению, в т.ч. в ответ на обращение Заявителя по заявлению
        /// </summary>
        /// <param name="code">Параметры текстового сообщения</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Post(InvitationEventParams parameters)
        {
            var request = new PushEventRequest
            {
                Message = ServiceSnippets.GetMessageHeader(),
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        orderId = parameters.OrderId,
                        eventComment = parameters.EventComment,
                        eventAuthor = parameters.EventAutor,
                        eventDate = parameters.EventDate.HasValue ? parameters.EventDate.Value : DateTime.Now,
                        @event = new EventObjectEvent
                        {
                            Item = new InvitationEvent
                            {
                                code = parameters.Code,
                                action = (InvitationEventAction)Enum.Parse(typeof(InvitationEventAction), parameters.Action, true),
                                address = parameters.Address,
                                startDate = parameters.StartDate,
                                endDate = parameters.EndDate.HasValue ? parameters.EndDate.Value : default(DateTime)
                            }
                        }
                    }
                }
            };
            XmlDocument response;
            try
            {
                response = RequestManager.GetResponse(request);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, XmlHelper.FormatXML(response));
        }
    }
}
