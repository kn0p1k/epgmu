﻿using EPGMU.StatementStatus;
using EPGMU.StatementStatus.EventService;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;

namespace EPGMU.Service.Controllers
{
#if DEBUG
    public class TestApiController : ApiController
    {
        // GET: TestApi
        public HttpResponseMessage Get()
        {

            var request = new PushEventRequest
            {
                Message = new MessageType
                {
                    Sender = new orgExternalType
                    {
                        Code = "043T01891",
                        Name = "ИС Единая информационная система по моделированию и прогнозированию социально - экономического развития коренных малочисленных народов Севера Ямало - Ненецкого автономного округа"
                    },
                    Recipient = new orgExternalType
                    {
                        Code = "IPGU01001",
                        Name = "Единый портал государственных услуг"
                    },
                    Originator = new orgExternalType
                    {
                        Code = "043T01891",
                        Name = "ИС Единая информационная система по моделированию и прогнозированию социально - экономического развития коренных малочисленных народов Севера Ямало - Ненецкого автономного округа"
                    },
                    TypeCode = TypeCodeType.GSRV,
                    Status = StatusType.RESULT,
                    Date = DateTime.Now,
                    ExchangeType = "1", // Категория взаимодействия
                    //OriginRequestIdRef = "12345678-0000-0000-0000-000000000001",
                    ServiceCode = "10001970310", // Код государственной услуги, в рамках оказания которой осуществляется информационный обмен
                    CaseNumber = "86980805", // Номер дела в информационной системе-отправителе
                    TestMsg = string.Empty
                },
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        orderId = 86980805,
                        eventDate = new DateTime(2015, 03, 30, 19, 10, 0),
                        eventComment = "Комментарий",
                        eventAuthor = "Автор",
                        @event = new EventObjectEvent
                        {
                            Item = new OrderStatusEvent
                            {
                                statusCode = new OrderStatusEventStatusCode
                                {
                                    Item = (long)7
                                },
                                cancelAllowed = true,
                                sendMessageAllowed = true
                            }
                        }
                    }
                }
            };
            XmlDocument response;
            try
            {
                response = RequestManager.GetResponse(request);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, FormatXML(response));
        }

        private string FormatXML(XmlDocument document)
        {
            string result = "";
            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            try
            {
                writer.Formatting = Formatting.Indented;
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                mStream.Position = 0;

                StreamReader sReader = new StreamReader(mStream);

                string FormattedXML = sReader.ReadToEnd();

                result = FormattedXML;
            }
            catch (Exception ex)
            {
                return "Ошибка при форматировании XML ответа: " + ex.Message;
            }
            return result;
        }


        [System.Web.Http.Route("api/TestApi/GetResponse")]
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetResponse([FromBody]JObject requestText)
        {
            var request = requestText["requestText"];
            XmlDocument response;
            Debug.WriteLine("Starting request...");
            try
            {
                response = RequestManager.GetResponse((string)request);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception throw: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, FormatXML(response));
        }
    }
#endif
}