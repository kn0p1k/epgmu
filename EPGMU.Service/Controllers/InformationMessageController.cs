﻿using EPGMU.Service.Common;
using EPGMU.StatementStatus;
using EPGMU.StatementStatus.EventService;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace EPGMU.Service.Controllers
{
    public class InformationMessageController : ApiController
    {
#if DEBUG
        /// <summary>
        /// Тест метода Post
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Post(new InformationMessageParams {
                OrderId = 86980805,
                EventComment = "Comment",
                EventAutor = "Autor",
                Code = "1"
            });
        }
#endif
        /// <summary>
        /// Передача приглашения на приём
        /// </summary>
        /// <param name="code">Параметры информационного сообщения</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Post(InformationMessageParams parameters)
        {
            var request = new PushEventRequest
            {
                Message = ServiceSnippets.GetMessageHeader(),
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        orderId = parameters.OrderId,
                        eventComment = parameters.EventComment,
                        eventAuthor = parameters.EventAutor,
                        eventDate = parameters.EventDate.HasValue ? parameters.EventDate.Value : DateTime.Now,
                        @event = new EventObjectEvent
                        {
                            Item = new InfoEvent
                            {
                                code = parameters.Code
                            }
                        }
                    }
                }
            };
            XmlDocument response;
            try
            {
                response = RequestManager.GetResponse(request);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, XmlHelper.FormatXML(response));
        }
    }
}
