﻿using EPGMU.Service.Common;
using EPGMU.StatementStatus;
using EPGMU.StatementStatus.EventService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;

namespace EPGMU.Service.Controllers
{
    public class StatementStatusController : ApiController
    {
#if DEBUG
        /// <summary>
        /// Тест остальных методов
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            return Put(new StatementStatusParams() {
                OrderId = 86980805,
                EventComment = "Comment",
                EventAutor = "Autor",
                StatusCode = 7,
                CancelAllowed = true,
                SendMessageAllowed = true
            });
        }
#endif

        /// <summary>
        /// Изменить статус заявления
        /// </summary>
        /// <param name="parameters">Параметры изменения статуса заявления: статус, комментарий, автор, признаки возможности отмены и посылки сообщений</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Put([FromBody]StatementStatusParams parameters)
        {
            var request = new PushEventRequest
            {
                Message = ServiceSnippets.GetMessageHeader(),
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        orderId = parameters.OrderId,
                        eventComment = parameters.EventComment,
                        eventAuthor = parameters.EventAutor,
                        eventDate = parameters.EventDate.HasValue ? parameters.EventDate.Value : DateTime.Now,
                        @event = new EventObjectEvent
                        {
                            Item = new OrderStatusEvent
                            {
                                statusCode = new OrderStatusEventStatusCode
                                {
                                    Item = parameters.StatusCode
                                },
                                cancelAllowed = parameters.CancelAllowed,
                                sendMessageAllowed = parameters.SendMessageAllowed
                            }
                        }
                    }
                }
            };
            XmlDocument response;
            try
            {
                response = RequestManager.GetResponse(request);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, XmlHelper.FormatXML(response));
        }

        

    }
}
