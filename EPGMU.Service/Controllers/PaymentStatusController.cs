﻿using EPGMU.Service.Common;
using EPGMU.StatementStatus;
using EPGMU.StatementStatus.EventService;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace EPGMU.Service.Controllers
{
    public class PaymentStatusController : ApiController
    {
#if DEBUG
        public HttpResponseMessage Get()
        {
            return Put(new PaymentStatusParams
            {
                OrderId = 86980805,
                EventComment = "Comment",
                EventAutor = "Autor",
                Status = "W",
                Uin = "19200000022577713012",
                Source = "FK",
                Description = "госпошлина"
            });
        }
#endif
        /// <summary>
        /// Изменить статус оплаты по заявлению на "ожидает оплаты" или "оплачено"
        /// </summary>
        /// <param name="parameters">Параметры изменения статуса оплаты: </param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Put([FromBody]PaymentStatusParams parameters)
        {
            var payment = new Payment
            {
                uin = parameters.Uin
            };
            var request = new PushEventRequest
            {
                Message = ServiceSnippets.GetMessageHeader(),
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        orderId = parameters.OrderId,
                        eventComment = parameters.EventComment,
                        eventAuthor = parameters.EventAutor,
                        eventDate = parameters.EventDate.HasValue ? parameters.EventDate.Value : DateTime.Now,
                        @event = new EventObjectEvent
                        {
                            Item = new PaymentStatusEvent
                            {
                                status = (PaymentStatus)Enum.Parse(typeof(PaymentStatus), parameters.Status, true),
                                payment = new Payment[]
                                {
                                    new Payment
                                    {
                                        uin = parameters.Uin,
                                        source = parameters.Source,
                                        description = parameters.Description
                                    }
                                }
                            }
                        }

                    }
                }
            };
            XmlDocument response;
            try
            {
                response = RequestManager.GetResponse(request);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, XmlHelper.FormatXML(response));
        }
    }
}
