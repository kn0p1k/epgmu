﻿using EPGMU.Service.Common;
using EPGMU.StatementStatus;
using EPGMU.StatementStatus.EventService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

namespace EPGMU.Service
{
    /// <summary>
    ///
    /// </summary>
    public class smev_srv : Ismev_srv
    {
        /// <summary>
        /// Получить статус заявки
        /// </summary>
        /// <param name="id">ключ заявки</param>
        /// <returns></returns>
        public ZayavStat getStatus(int id)
        {
            try
            {
                DataTable dt = Utils.GetDataTable(string.Format(@"SELECT *
                                                                      FROM (  SELECT a.calendar, b.name, a.ZAYAVKA_ID
                                                                                FROM TBL_STAT_HISTORY a, TBL_STATS b
                                                                               WHERE a.stat_id = B.STAT_ID AND ZAYAVKA_ID = {0}
                                                                            ORDER BY calendar DESC)
                                                                     WHERE ROWNUM = 1", id));
                if (dt.Rows.Count > 0)
                {
                    return new ZayavStat()
                    {
                        calendar = Convert.ToDateTime(dt.Rows[0]["calendar"]),
                        status = dt.Rows[0]["name"].ToString(),
                        zayavka_id = id
                    };
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Получить историю статусов заявки
        /// </summary>
        /// <param name="id">ключ заявки</param>
        /// <returns></returns>
        public List<ZayavStat> getStatusHistory(int id)
        {
            try
            {
                DataTable dt = Utils.GetDataTable(string.Format(@"SELECT a.calendar, b.name, a.ZAYAVKA_ID
                                                                                FROM TBL_STAT_HISTORY a, TBL_STATS b
                                                                               WHERE a.stat_id = B.STAT_ID AND ZAYAVKA_ID = {0}
                                                                            ORDER BY calendar DESC", id));
                if (dt.Rows.Count > 0)
                {
                    List<ZayavStat> list = new List<ZayavStat>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        list.Add(new ZayavStat()
                        {
                            calendar = Convert.ToDateTime(dt.Rows[i]["calendar"]),
                            status = dt.Rows[i]["name"].ToString(),
                            zayavka_id = id
                        });
                    }

                    return list;
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public PushEventRequest request;

        /// <summary>
        /// Добавить заявку
        /// </summary>
        /// <param name="request">Заявка</param>
        /// <returns></returns>
        public bool addZayavka(PushEventRequest request)
        {
            try
            {
                string zayavka_id = Utils.GetQueryResult("select zayavka_seq.nextval from dual").ToString();

                request.MessageData.AppData.orderId = Convert.ToInt64(zayavka_id);
                string user = request.MessageData.AppData.eventAuthor;

                Utils.ExecuteQuery(string.Format(@"insert into TBL_ZAYAVKA(zayavka_id, user_id) values({0}, '{1}')", zayavka_id, user));
                Utils.ExecuteQuery(string.Format(@"insert into TBL_STAT_HISTORY(calendar, stat_id, zayavka_id) values(sysdate, 1, {0})", zayavka_id));

                XmlDocument response;

                try
                {
                    // получаем ответ, куда его потом девать хз....
                    response = RequestManager.GetResponse(request);
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}