﻿using EPGMU.StatementStatus.EventService;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace EPGMU.Service
{
    [ServiceContract]
    public interface Ismev_srv
    {
        /// <summary>
        /// Актуальный статус по заявке
        /// </summary>
        /// <param name="zayavka_id">ключ заявки</param>
        /// <returns></returns>
        [OperationContract]
        ZayavStat getStatus(int zayavka_id);

        /// <summary>
        /// История статусов по заявке
        /// </summary>
        /// <param name="zayavka_id">ключ заявки</param>
        /// <returns></returns>
        [OperationContract]
        List<ZayavStat> getStatusHistory(int zayavka_id);

        /// <summary>
        /// Добавить заявку
        /// </summary>
        /// <param name="request">Заявка</param>
        /// <returns></returns>
        [OperationContract]
        bool addZayavka(PushEventRequest request);
    }

    /// <summary>
    /// Статус заявки
    /// </summary>
    public class ZayavStat
    {
        /// <summary>
        /// Дата
        /// </summary>
        public DateTime calendar { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// ID заявки
        /// </summary>
        public int zayavka_id { get; set; }
    }
}