﻿using EPGMU.StatementStatus.EventService;
using System;

namespace smev_client
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            smev_srv.Ismev_srvClient cl = new smev_srv.Ismev_srvClient();
            
            var request = new PushEventRequest
            {
                Message = new MessageType
                {
                    Sender = new orgExternalType
                    {
                        Code = "043T01891",
                        Name = "ИС Единая информационная система по моделированию и прогнозированию социально - экономического развития коренных малочисленных народов Севера Ямало - Ненецкого автономного округа"
                    },
                    Recipient = new orgExternalType
                    {
                        Code = "IPGU01001",
                        Name = "Единый портал государственных услуг"
                    },
                    Originator = new orgExternalType
                    {
                        Code = "043T01891",
                        Name = "ИС Единая информационная система по моделированию и прогнозированию социально - экономического развития коренных малочисленных народов Севера Ямало - Ненецкого автономного округа"
                    },
                    TypeCode = TypeCodeType.GSRV,
                    Status = StatusType.RESULT,
                    Date = DateTime.Now,
                    ExchangeType = "1", // Категория взаимодействия
                    //OriginRequestIdRef = "12345678-0000-0000-0000-000000000001",
                    ServiceCode = "10001970310", // Код государственной услуги, в рамках оказания которой осуществляется информационный обмен
                    CaseNumber = "86980805", // Номер дела в информационной системе-отправителе
                    TestMsg = string.Empty
                },
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        //orderId = 86980805,
                        eventDate = new DateTime(2015, 03, 30, 19, 10, 0),
                        eventComment = "Комментарий",
                        eventAuthor = "Автор",
                        //@event = new EventObjectEvent
                        //{
                        //    Item = new OrderStatusEvent
                        //    {
                        //        statusCode = new OrderStatusEventStatusCode
                        //        {
                        //            Item = (long)7
                        //        },
                        //        cancelAllowed = true,
                        //        sendMessageAllowed = true
                        //    }
                        //}
                    }
                }
            };

            try
            {
                if (cl.addZayavka(request))
                {
                    Console.WriteLine("request ok");
                }
                else
                {
                    Console.WriteLine("request err");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}