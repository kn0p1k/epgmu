﻿using EPGMU.StatementStatus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace EPGMU.SignTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            textBox.Text = EPGMU.SignTestApp.Properties.Resources.signTestXML;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var request = textBox.Text;
            XmlDocument response = null;
            Debug.WriteLine("Starting request...");
            try
            {
                response = RequestManager.GetResponse((string)request);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception throw: " + ex.Message);
                //MessageBox.Show("Exception: " + ex.Message);
                textBoxResponse.Text = "Exception: " + ex.Message;
            }

            //MessageBox.Show(FormatXML(response));
            textBoxResponse.Text = FormatXML(response);
        }

        private string FormatXML(XmlDocument document)
        {
            string result = "";
            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            try
            {
                writer.Formatting = Formatting.Indented;
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                mStream.Position = 0;

                StreamReader sReader = new StreamReader(mStream);

                string FormattedXML = sReader.ReadToEnd();

                result = FormattedXML;
            }
            catch (Exception ex)
            {
                return "Ошибка при форматировании XML ответа: " + ex.Message;
            }
            return result;
        }
    }
}
