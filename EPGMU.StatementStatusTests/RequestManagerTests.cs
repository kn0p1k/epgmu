﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EPGMU.StatementStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPGMU.StatementStatus.EventService;

namespace EPGMU.StatementStatus.Tests
{
    [TestClass()]
    public class RequestManagerTests
    {
        [TestMethod()]
        public void GetResponseTest()
        {
            var request = new PushEventRequest
            {
                Message = new MessageType
                {
                    Sender = new orgExternalType
                    {
                        Code = "IPGU01001",
                        Name = "Единый портал государственных услуг"
                    },
                    Recipient = new orgExternalType
                    {
                        Code = "IPGU01001",
                        Name = "Единый портал государственных услуг"
                    },
                    Originator = new orgExternalType
                    {
                        Code = "IPGU01001",
                        Name = "Единый портал государственных услуг"
                    },
                    TypeCode = TypeCodeType.GSRV,
                    Status = StatusType.RESULT,
                    Date = DateTime.Now,
                    ExchangeType = "1",
                    OriginRequestIdRef = "12345678-0000-0000-0000-000000000001",
                    ServiceCode = "10001970310",
                    CaseNumber = "86980805",
                    TestMsg = string.Empty
                },
                MessageData = new EventMessageDataType
                {
                    AppData = new EventAppDataType
                    {
                        orderId = 86980805,
                        eventDate = new DateTime(2015, 03, 30, 19, 10, 0),
                        eventComment = "Комментарий",
                        eventAuthor = "Автор",
                        @event = new EventObjectEvent
                        {
                            Item = new OrderStatusEvent
                            {
                                statusCode = new OrderStatusEventStatusCode
                                {
                                    Item = (long)7
                                },
                                cancelAllowed = true,
                                sendMessageAllowed = true
                            }
                        }
                    }
                }
            };
            var response = RequestManager.GetResponse(request);
        }
    }
}