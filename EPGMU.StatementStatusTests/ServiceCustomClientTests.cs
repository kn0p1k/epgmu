﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EPGMU.StatementStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPGMU.StatementStatusTests.Properties;
using System.Xml;
using GostCryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using GostCryptography.Cryptography;
using System.IO;
using System.Xml.Serialization;
using EPGMU.StatementStatus.EventService;

namespace EPGMU.StatementStatus.Tests
{
    [TestClass()]
    public class ServiceCustomClientTests
    {
        private const string WsSecurityExtNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        private const string WsSecurityUtilityNamespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

        [TestMethod()]
        public void ServiceCustomClientTest()
        {

            var client = new ServiceCustomClient();
            Assert.IsFalse(string.IsNullOrWhiteSpace(client.ServiceUri.OriginalString));
        }

        [TestMethod()]
        public void SendMessageTest()
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(Resources.ChangeStatusExample1);
            var cert = CertificateHelper.GetCertificate();
            var signedTestMessage = SignSmevRequest(xmlDocument, cert);
            //var str = SerializeXml(signedTestMessage);
            var serviceClient = new ServiceCustomClient();
            var res = serviceClient.SendMessage(signedTestMessage);
            var message = res.GetElementsByTagName("ns5:pushEventResponse");
            //if (message.Count > 0)
            //{
            //    MemoryStream stm = new MemoryStream();

            //    StreamWriter stw = new StreamWriter(stm);
            //    stw.Write(message[0].OuterXml);
            //    stw.Flush();

            //    stm.Position = 0;

            //    XmlSerializer ser = new XmlSerializer(typeof(PushEventResponse), "http://idecs.atc.ru/orderprocessing/ws/eventservice/v25/");
            //    PushEventResponse result = (ser.Deserialize(stm) as PushEventResponse);
            //}
        }

        private string SerializeXml(XmlDocument xmlDoc)
        {
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                xmlDoc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }

        private XmlDocument SignSmevRequest(XmlDocument smevRequest, X509Certificate2 signingCertificate)
        {
            // Создание подписчика XML-документа
            var signedXml = new GostSignedXml(smevRequest) { GetIdElementHandler = GetSmevIdElement };

            // Установка ключа для создания подписи
            signedXml.SetSigningCertificate(signingCertificate);

            // Ссылка на узел, который нужно подписать, с указанием алгоритма хэширования ГОСТ Р 34.11-94 (в соответствии с методическими рекомендациями СМЭВ)
            var dataReference = new Reference { Uri = "#body", DigestMethod = GostSignedXml.XmlDsigGost3411ObsoleteUrl };

            // Метод преобразования, применяемый к данным перед их подписью (в соответствии с методическими рекомендациями СМЭВ)
            var dataTransform = new XmlDsigExcC14NTransform();
            dataReference.AddTransform(dataTransform);

            // Установка ссылки на узел
            signedXml.AddReference(dataReference);

            // Установка алгоритма нормализации узла SignedInfo (в соответствии с методическими рекомендациями СМЭВ)
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

            // Установка алгоритма подписи ГОСТ Р 34.10-2001 (в соответствии с методическими рекомендациями СМЭВ)
            signedXml.SignedInfo.SignatureMethod = GostSignedXml.XmlDsigGost3410ObsoleteUrl;

            // Вычисление подписи
            signedXml.ComputeSignature();

            // Получение XML-представления подписи
            var signatureXml = signedXml.GetXml();

            // Добавление подписи в исходный документ
            smevRequest.GetElementsByTagName("ds:Signature")[0].PrependChild(smevRequest.ImportNode(signatureXml.GetElementsByTagName("SignatureValue")[0], true));
            smevRequest.GetElementsByTagName("ds:Signature")[0].PrependChild(smevRequest.ImportNode(signatureXml.GetElementsByTagName("SignedInfo")[0], true));
            smevRequest.GetElementsByTagName("wsse:BinarySecurityToken")[0].InnerText = Convert.ToBase64String(signingCertificate.RawData);

            return smevRequest;
        }

        private static bool VerifySmevRequestSignature(XmlDocument signedSmevRequest)
        {
            // Создание подписчика XML-документа
            var signedXml = new GostSignedXml(signedSmevRequest) { GetIdElementHandler = GetSmevIdElement };

            // Поиск узла с подписью
            var nodeList = signedSmevRequest.GetElementsByTagName("Signature", SignedXml.XmlDsigNamespaceUrl);

            // Загрузка найденной подписи
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Поиск ссылки на BinarySecurityToken
            var references = signedXml.KeyInfo.GetXml().GetElementsByTagName("Reference", WsSecurityExtNamespace);

            if (references.Count > 0)
            {
                // Определение ссылки на сертификат (ссылка на узел документа)
                var binaryTokenReference = ((XmlElement)references[0]).GetAttribute("URI");

                if (!String.IsNullOrEmpty(binaryTokenReference) && binaryTokenReference[0] == '#')
                {
                    // Поиск элемента с закодированным в Base64 сертификатом
                    var binaryTokenElement = signedXml.GetIdElement(signedSmevRequest, binaryTokenReference.Substring(1));

                    if (binaryTokenElement != null)
                    {
                        // Загрузка сертификата, который был использован для подписи
                        var signingCertificate = new X509Certificate2(Convert.FromBase64String(binaryTokenElement.InnerText));

                        // Проверка подписи
                        return signedXml.CheckSignature(signingCertificate.GetPublicKeyAlgorithm());
                    }
                }
            }

            return false;
        }

        private static XmlElement GetSmevIdElement(XmlDocument document, string idValue)
        {
            var namespaceManager = new XmlNamespaceManager(document.NameTable);
            namespaceManager.AddNamespace("wsu", WsSecurityUtilityNamespace);

            return document.SelectSingleNode("//*[@wsu:Id='" + idValue + "']", namespaceManager) as XmlElement;
        }


    }
}